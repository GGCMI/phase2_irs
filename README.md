## Set of R scripts for data processing

author: Christoph Müller, PIK


### License

AGPLv3, see file `LICENSE`.

### Purpose

Data processing scripts to analyze a large ensemble of simulated crop yield data from the GGCMI[1] Phase2 experiment[2]. The analysis based on these scripts is published by Müller et al.[3] and this archive of scripts is meant to increase transparency and reproducibility. 

### Usage

The scripts can be run as is, but require 
* a version of R, compatible with the scripts (I used version 4.1.2)
* installation of all R libraries used 
* access to the raw GGCMI output data, as referenced in the paper[3]
* adjusting of paths

There are some interdependencies, also indicated in the comments in the scripts.
Before you will be able to run 
* `ggcmi_read_data_for_pixel_irs.R` or 
* `ggcmi_irs_clustering.R`  

you will have to run

* `ggcmi_create_data_for_pixel_irs.R`

which generates the pre-processed data needed.

The file `runR_slurm_largemem.jcf` is a job description file for submitting a script to the HPC2015 cluster at PIK.

[1]: https://agmip.org/ag-grid-2/
[2]: http://dx.doi.org/10.5194/gmd-13-2315-2020
[3]: https://dx.doi.org/10.22541/essoar.168394775.56087254/v1
